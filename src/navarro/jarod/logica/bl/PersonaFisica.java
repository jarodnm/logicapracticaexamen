package navarro.jarod.logica.bl;

public class PersonaFisica extends Persona {
    private String apellidos;

    public PersonaFisica() {
        super();
    }

    public PersonaFisica(String nombre, String direccion, String telefono, String id, String apellidos) {
        super(nombre, direccion, telefono, id);
        this.apellidos = apellidos;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Override
    public String toString() {
        return
                super.toString()+  ", apellidos: " + apellidos + ", El proovedor es una Persona Fisica";
    }
}
