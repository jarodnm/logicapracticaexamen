package navarro.jarod.logica.bl;

import java.util.ArrayList;

public class Carro {
    private String idCliente;
    private ArrayList<Producto> productos = new ArrayList<>();

    public Carro() {
    }

    public Carro(String idCliente, ArrayList<Producto> productos) {
        this.idCliente = idCliente;
        this.productos = productos;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void setProductos(Producto tmpproducto) {
        productos.add(tmpproducto);
    }

    @Override
    public String toString() {
        return
                "idCliente: " + idCliente +
                        ", productos: " + productos
                ;
    }
}
