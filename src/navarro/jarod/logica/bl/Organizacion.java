package navarro.jarod.logica.bl;

public class Organizacion extends Persona {
    private String representante;

    public Organizacion() {
       super();
    }

    public Organizacion(String nombre, String direccion, String telefono, String id, String representante) {
        super(nombre, direccion, telefono, id);
        this.representante = representante;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    @Override
    public String toString() {
        return
                super.toString()+", representante: " + representante + ", El proovedor es una organización";

    }
}

