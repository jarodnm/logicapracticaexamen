package navarro.jarod.logica.bl;

public class Producto {
    private int codigo;
    private String descripcion;
    private double precioOrganico;
    private double precioNoOrganico;
    private boolean tipo;

    public Producto() {
    }

    public Producto(int codigo, String descripcion, double precioOrganico, double precioNoOrganico, boolean tipo) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precioOrganico = precioOrganico;
        this.precioNoOrganico = precioNoOrganico;
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioOrganico() {
        return precioOrganico;
    }

    public void setPrecioOrganico(double precioOrganico) {
        this.precioOrganico = precioOrganico;
    }

    public double getPrecioNoOrganico() {
        return precioNoOrganico;
    }

    public void setPrecioNoOrganico(double precioNoOrganico) {
        this.precioNoOrganico = precioNoOrganico;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        String tipoP;
        if(tipo){
            tipoP = "Fruta";
        }else{
            tipoP= "Verdura";

        }
        return
                "Codigo: " + codigo +
                        ", descripcion: " + descripcion +
                        ", precioOrganico: " + precioOrganico +
                        ", precioNoOrganico: " + precioNoOrganico +
                        ", tipo: " + tipoP
                ;
    }
}
