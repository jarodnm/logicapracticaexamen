package navarro.jarod.logica.bl;

public class Cliente extends Persona {
    private String provincia;
    private String canton;
    private String disctrito;
    private String correo;

    public Cliente() {
        super();
    }

    public Cliente(String nombre, String direccion, String telefono, String id, String provincia, String canton, String disctrito, String correo) {
        super(nombre, direccion, telefono, id);
        this.provincia = provincia;
        this.canton = canton;
        this.disctrito = disctrito;
        this.correo = correo;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDisctrito() {
        return disctrito;
    }

    public void setDisctrito(String disctrito) {
        this.disctrito = disctrito;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return
                super.toString()+", provincia: " + provincia +
                        ", canton: " + canton +
                        ", disctrito: " + disctrito +
                        ", correo: " + correo ;
    }
}
