package navarro.jarod.logica.tl;
import navarro.jarod.logica.bl.*;
import navarro.jarod.logica.dl.*;
public class Controller {
    CL capa = new CL();

    public void registrarOrganización(String nombre, String direccion, String telefono, String id, String representante){
        Organizacion tmporganizacion = new Organizacion(nombre,direccion,telefono,id,representante);
        capa.registrarOrganizacion(tmporganizacion);
    }

    public void registrarPersonaFisica(String nombre, String direccion, String telefono, String id, String apellidos){
        PersonaFisica tmppf = new PersonaFisica(nombre,direccion,telefono,id,apellidos);
        capa.registrarPersonaFisica(tmppf);
    }

    public void crearCarro(String id){
        Carro tmpcarro = new Carro();
        tmpcarro.setIdCliente(id);
        capa.crearCarro(tmpcarro);
    }

    public void registrarProducto(int codigo, String descripcion, double precioOrganico, double precioNoOrganico, boolean tipo){
        Producto tmpproducto = new Producto(codigo,descripcion,precioOrganico,precioNoOrganico,tipo);
        capa.registrarProducto(tmpproducto);
    }

    public void registrarCliente(String nombre, String direccion, String telefono, String id, String provincia, String canton, String disctrito, String correo){
        Cliente tmpcliente = new Cliente(nombre,direccion,telefono,id,provincia,canton,disctrito,correo );
        capa.registrarCliente(tmpcliente);
    }

    public void meterProducto(String id, int codigo){
        capa.meterProducto(id,codigo);
    }

    public boolean productoExiste(int codigo){
        return capa.productoExiste(codigo);
    }

    public boolean proovedorExiste(String id){
        return capa.proovedorExiste(id);
    }

    public boolean clienteExiste(String id){
        return capa.clienteExiste(id);
    }

    public boolean carroExiste(String id){
       return capa.carroExiste(id);
    }

    public String buscarProducto (int codigo){
        return capa.buscarProducto(codigo);
    }

    public String [] listarProveedores(){
        return capa.listarProveedores();
    }

    public String [] listarProductos(){
        return capa.listarProductos();
    }

    public String listarCarro(String id){
        return capa.listarCarro(id);
    }

    public String[] listarClientes (){
        return capa.listarClientes();
    }



}
