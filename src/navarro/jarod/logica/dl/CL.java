package navarro.jarod.logica.dl;

import navarro.jarod.logica.bl.*;

import java.util.ArrayList;

public class CL {
    static ArrayList<Persona> proveedores = new ArrayList<>();
    static ArrayList<Cliente> clientes = new ArrayList<>();
    static ArrayList<Producto> productos = new ArrayList<>();
    static ArrayList<Carro> carros = new ArrayList<>();

    public void registrarOrganizacion(Organizacion tmpOrganizacion) {
        proveedores.add(tmpOrganizacion);
    }

    public void registrarPersonaFisica(PersonaFisica tmppf) {
        proveedores.add(tmppf);
    }

    public void crearCarro(Carro tmpcarro) {
        carros.add(tmpcarro);
    }

    public void registrarProducto(Producto tmpproducto){
        productos.add(tmpproducto);
    }

    public void registrarCliente(Cliente tmpcliente){
        clientes.add(tmpcliente);
    }

    public void meterProducto(String id,int codigo){
       for(Carro dato : carros){
           for(Producto datoP : productos){
               if(dato.getIdCliente().equals(id)&&datoP.getCodigo() == codigo){
                   dato.setProductos(datoP);
               }
           }
       }
    }

    public boolean productoExiste(int codigo){
        boolean existe = false;
        for(Producto dato:productos){
            if(dato.getCodigo() == codigo){
                existe = true;
            }
        }
        return existe;
    }

    public boolean proovedorExiste(String id ){
        boolean existe = false;
        for(Persona dato : proveedores){
            if(dato.getId().equals(id)){
                existe = true;
            }
        }
        return existe;
    }

    public boolean clienteExiste(String id){
        boolean existe = false;
        for(Cliente dato : clientes){
            if(dato.getId().equals(id)){
                existe = true;
            }
        }
        return existe;
    }

    public boolean carroExiste(String id){
       boolean existe = false;
        for(Carro dato: carros){
            if(dato.getIdCliente().equals(id)){
                existe = true;
            }
        }
        return existe;
    }

    public String buscarProducto(int codigo){
        String producto = "El producto digitado no existe";
        for(Producto dato:productos){
            if(dato.getCodigo() == codigo){
                producto = dato.toString();
            }
        }
        return producto;
    }

    public String [] listarProveedores(){
        String todoProveedor[] = new String[proveedores.size()];
        int pos = 0;
        for(Persona dato : proveedores){
            todoProveedor[pos] = dato.toString();
            pos++;
        }
        return todoProveedor;
    }

    public String[] listarProductos(){
        String todoProducto[] = new String [productos.size()];
        int pos = 0;
        for(Producto dato : productos){
            todoProducto[pos] = dato.toString();
            pos++;
        }
        return todoProducto;
    }

    public String listarCarro(String id){
        String carro = "Usted no tiene un carro de compras";
        for(Carro dato: carros){
            if(dato.getIdCliente().equals(id)){
                carro = dato.toString();
            }
        }
        return carro;
    }

    public String[] listarClientes (){
        String[] todoCliente = new String[clientes.size()];
        int pos = 0;
        for(Cliente dato : clientes){
            todoCliente[pos] = dato.toString();
            pos++;
        }
        return todoCliente;
    }
}

